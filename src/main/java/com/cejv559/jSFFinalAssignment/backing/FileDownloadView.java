/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.backing;

import java.io.InputStream;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author uwij002
 */
@Named
@RequestScoped
public class FileDownloadView {
    
    
 private StreamedContent file;
 @Inject
 private LoginController loginController;
 
     
    public FileDownloadView() {  
    }
 
    public StreamedContent getFile() {
       String fileName =  loginController.getBooks().getTitle() + ".zip";
       InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/eBooks/MobyDick-h.zip");
        file = new DefaultStreamedContent(stream, "zip", fileName);
        return file;
        
    }  
    

    
}
