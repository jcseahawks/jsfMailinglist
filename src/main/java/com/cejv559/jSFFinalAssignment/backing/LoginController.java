/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.backing;

import com.cejv559.jSFFinalAssignment.entities.Books;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author uwij002
 */
@Named
@SessionScoped
public class LoginController implements Serializable{

    @Inject
    private UsersBackingBean usersBackingBean;
    private String outcome;
    private String email;
    private String password;
    private Books books;

    public String login() {
        
        outcome = checkPassword();
        return outcome;

    }
    
    public String checkPassword(){
        FacesContext fc = FacesContext.getCurrentInstance();
        
        if(isRegistered(email)){
         if(usersBackingBean.getUserByEmail(email).getPassword().equals(password)){ 
         getBookInfo();
         outcome = "download";
         }else if(!usersBackingBean.getUserByEmail(email).getPassword().equals(password)){
         fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong username or password", null));
         outcome = "login";
         }
         
        }else{
        
        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong username or password", null));
        outcome = "login";
        }
        return outcome;
    
    
    }
    
    public void getBookInfo(){
    books = usersBackingBean.getUserByEmail(email)
                            .getBookTitle();
    
    
    }
    
    
    
    public boolean isRegistered(String email){
        
        return email.equalsIgnoreCase((usersBackingBean.getUserByEmail(email)!= null)?usersBackingBean.getUserByEmail(email).getEmail(): "");
    
    
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Books getBooks() {
        return books;
    }
    
    

}
