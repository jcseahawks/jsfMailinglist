/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.backing;

import com.cejv559.jSFFinalAssignment.entities.Books;
import com.cejv559.jSFFinalAssignment.jpaControllers.BooksJpaController;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author uwij002
 */
@Named
@RequestScoped
public class BooksBackingBean {
    @Inject
    private BooksJpaController booksJpaController;
    
    public List<Books> getBooks(){
        return booksJpaController.findBooksEntities();
    
    }
      
    
}
