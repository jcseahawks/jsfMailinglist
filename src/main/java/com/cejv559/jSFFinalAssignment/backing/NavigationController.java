/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.backing;

import com.cejv559.jSFFinalAssignment.entities.Books;
import com.cejv559.jSFFinalAssignment.jpaControllers.BooksJpaController;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author uwij002
 */
@Named
@SessionScoped
public class NavigationController implements Serializable {

    @Inject
    private BooksJpaController booksJpaController;
    private Books books;

    public String register() throws ParseException {
        books = new Books();
        SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        FacesContext fc = FacesContext.getCurrentInstance();

        Map<String, String> booksParamMap = getBookParamMap(fc);

        books = booksJpaController.findBooks(booksParamMap.get("booksTitle"));

        return "register";

    }

    public Map<String, String> getBookParamMap(FacesContext fc) {

        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

        return params;

    }

    public Books getBooks() {
        return books;
    }

}
