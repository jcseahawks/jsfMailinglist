/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.backing;

import com.cejv559.jSFFinalAssignment.entities.Users;
import com.cejv559.jSFFinalAssignment.jpaControllers.UsersJpaController;
import com.cejv559.jSFFinalAssignment.smtp.EmailSender;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author uwij002
 */
@Named("usersEntryBean")
@RequestScoped
public class UsersIOBackingBean {

    @Inject
    private UsersJpaController usersJpaController;
    @Inject
    private NavigationController navigationController;
    @Inject
    private EmailSender emailSender;
    private Users users;

    public Users getUsers() {

        if (users == null) {
            users = new Users();
        }
        return users;

    }

    /**
     * Save the current user to the db
     *
     * @return
     * @throws Exception
     */
    public String createUser() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
       
        ResourceBundle msg = ResourceBundle.getBundle("com.cejv559.bundles.messages");
        
        boolean isExist = usersJpaController.findUsers(users.getEmail()) != null;
        
        if (isExist) {

            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email address already registered", null));
            return "";
        }
        users.setBookTitle(navigationController.getBooks());
        usersJpaController.create(users);
        String body = msg.getString("messageGreeting") +" " + users.getFirstName() +",\n\n" + msg.getString("messageBody") + " " +users.getBookTitle().getTitle() + ".";
        emailSender.send("info.cejv559@gmail.com", "concordia", users.getEmail(), msg.getString("messageSubject"), body, msg.getString("messageName"));
        return "login";

    }

    /**
     * Update the current record Not implemented
     *
     * @throws Exception
     */
    public void updateUser() throws Exception {
        usersJpaController.edit(users);
    }

}
