/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.backing;

import com.cejv559.jSFFinalAssignment.entities.Users;
import com.cejv559.jSFFinalAssignment.jpaControllers.UsersJpaController;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author uwij002
 */
@Named
@RequestScoped
public class UsersBackingBean {
    
    @Inject
    private UsersJpaController usersJpaController;
    
    
    public List<Users> getUsers(){
        return usersJpaController.findUsersEntities();
    
    }
    
    public Users getUserByEmail(String email){
        
        Users result = getUsers().stream()
                       .filter(usersObj -> email.equalsIgnoreCase(usersObj.getEmail()))
                       .findAny()
                       .orElse(null);
                       
        
        return result;
    
    
    }
    
    
}
