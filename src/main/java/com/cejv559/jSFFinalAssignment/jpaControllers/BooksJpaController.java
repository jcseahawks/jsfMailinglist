/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.jpaControllers;

import com.cejv559.jSFFinalAssignment.entities.Books;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.cejv559.jSFFinalAssignment.entities.Users;
import com.cejv559.jSFFinalAssignment.jpaControllers.exceptions.IllegalOrphanException;
import com.cejv559.jSFFinalAssignment.jpaControllers.exceptions.NonexistentEntityException;
import com.cejv559.jSFFinalAssignment.jpaControllers.exceptions.PreexistingEntityException;
import com.cejv559.jSFFinalAssignment.jpaControllers.exceptions.RollbackFailureException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 *
 * @author uwij002
 */
public class BooksJpaController implements Serializable {

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    public void create(Books books) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (books.getUsersCollection() == null) {
            books.setUsersCollection(new ArrayList<>());
        }
        
        try {
            utx.begin();
            
            Collection<Users> attachedUsersCollection = new ArrayList<>();
            books.getUsersCollection().stream().map((usersCollectionUsersToAttach) -> em.getReference(usersCollectionUsersToAttach.getClass(), usersCollectionUsersToAttach.getEmail())).forEachOrdered((usersCollectionUsersToAttach) -> {
                attachedUsersCollection.add(usersCollectionUsersToAttach);
            });
            books.setUsersCollection(attachedUsersCollection);
            em.persist(books);
            books.getUsersCollection().forEach((usersCollectionUsers) -> {
                Books oldBookTitleOfUsersCollectionUsers = usersCollectionUsers.getBookTitle();
                usersCollectionUsers.setBookTitle(books);
                usersCollectionUsers = em.merge(usersCollectionUsers);
                if (oldBookTitleOfUsersCollectionUsers != null) {
                    oldBookTitleOfUsersCollectionUsers.getUsersCollection().remove(usersCollectionUsers);
                    oldBookTitleOfUsersCollectionUsers = em.merge(oldBookTitleOfUsersCollectionUsers);
                }
            });
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findBooks(books.getTitle()) != null) {
                throw new PreexistingEntityException("Books " + books + " already exists.", ex);
            }
            throw ex;
        } 
       
    }

    public void edit(Books books) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
       
        try {
            utx.begin();
            
            Books persistentBooks = em.find(Books.class, books.getTitle());
            Collection<Users> usersCollectionOld = persistentBooks.getUsersCollection();
            Collection<Users> usersCollectionNew = books.getUsersCollection();
            List<String> illegalOrphanMessages = null;
            for (Users usersCollectionOldUsers : usersCollectionOld) {
                if (!usersCollectionNew.contains(usersCollectionOldUsers)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<>();
                    }
                    illegalOrphanMessages.add("You must retain Users " + usersCollectionOldUsers + " since its bookTitle field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Users> attachedUsersCollectionNew = new ArrayList<>();
            for (Users usersCollectionNewUsersToAttach : usersCollectionNew) {
                usersCollectionNewUsersToAttach = em.getReference(usersCollectionNewUsersToAttach.getClass(), usersCollectionNewUsersToAttach.getEmail());
                attachedUsersCollectionNew.add(usersCollectionNewUsersToAttach);
            }
            usersCollectionNew = attachedUsersCollectionNew;
            books.setUsersCollection(usersCollectionNew);
            books = em.merge(books);
            for (Users usersCollectionNewUsers : usersCollectionNew) {
                if (!usersCollectionOld.contains(usersCollectionNewUsers)) {
                    Books oldBookTitleOfUsersCollectionNewUsers = usersCollectionNewUsers.getBookTitle();
                    usersCollectionNewUsers.setBookTitle(books);
                    usersCollectionNewUsers = em.merge(usersCollectionNewUsers);
                    if (oldBookTitleOfUsersCollectionNewUsers != null && !oldBookTitleOfUsersCollectionNewUsers.equals(books)) {
                        oldBookTitleOfUsersCollectionNewUsers.getUsersCollection().remove(usersCollectionNewUsers);
                        oldBookTitleOfUsersCollectionNewUsers = em.merge(oldBookTitleOfUsersCollectionNewUsers);
                    }
                }
            }
            utx.commit();
        } catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = books.getTitle();
                if (findBooks(id) == null) {
                    throw new NonexistentEntityException("The books with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
     
        try {
            utx.begin();
            
            Books books;
            try {
                books = em.getReference(Books.class, id);
                books.getTitle();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The books with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Users> usersCollectionOrphanCheck = books.getUsersCollection();
            for (Users usersCollectionOrphanCheckUsers : usersCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<>();
                }
                illegalOrphanMessages.add("This Books (" + books + ") cannot be destroyed since the Users " + usersCollectionOrphanCheckUsers + " in its usersCollection field has a non-nullable bookTitle field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(books);
            utx.commit();
        } catch (IllegalOrphanException | NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }

    public List<Books> findBooksEntities() {
        return findBooksEntities(true, -1, -1);
    }

    public List<Books> findBooksEntities(int maxResults, int firstResult) {
        return findBooksEntities(false, maxResults, firstResult);
    }

    private List<Books> findBooksEntities(boolean all, int maxResults, int firstResult) {
        
   
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Books.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
      
    }

    public Books findBooks(String id) {
        
     
            return em.find(Books.class, id);
        
    }

    public int getBooksCount() {
       
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Books> rt = cq.from(Books.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
    }
    
}
