/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv559.jSFFinalAssignment.jpaControllers;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.cejv559.jSFFinalAssignment.entities.Books;
import com.cejv559.jSFFinalAssignment.entities.Users;
import com.cejv559.jSFFinalAssignment.jpaControllers.exceptions.NonexistentEntityException;
import com.cejv559.jSFFinalAssignment.jpaControllers.exceptions.PreexistingEntityException;
import com.cejv559.jSFFinalAssignment.jpaControllers.exceptions.RollbackFailureException;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 *
 * @author uwij002
 */
public class UsersJpaController implements Serializable {

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;
    
    public void create(Users users) throws PreexistingEntityException, RollbackFailureException, Exception {
      
        try {
            utx.begin();
          
            Books bookTitle = users.getBookTitle();
            if (bookTitle != null) {
                bookTitle = em.getReference(bookTitle.getClass(), bookTitle.getTitle());
                users.setBookTitle(bookTitle);
            }
            em.persist(users);
            if (bookTitle != null) {
                bookTitle.getUsersCollection().add(users);
                bookTitle = em.merge(bookTitle);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findUsers(users.getEmail()) != null) {
                throw new PreexistingEntityException("Users " + users + " already exists.", ex);
            }
            throw ex;
        } 
        
    }

    public void edit(Users users) throws NonexistentEntityException, RollbackFailureException, Exception {
        
        try {
            utx.begin();
            
            Users persistentUsers = em.find(Users.class, users.getEmail());
            Books bookTitleOld = persistentUsers.getBookTitle();
            Books bookTitleNew = users.getBookTitle();
            if (bookTitleNew != null) {
                bookTitleNew = em.getReference(bookTitleNew.getClass(), bookTitleNew.getTitle());
                users.setBookTitle(bookTitleNew);
            }
            users = em.merge(users);
            if (bookTitleOld != null && !bookTitleOld.equals(bookTitleNew)) {
                bookTitleOld.getUsersCollection().remove(users);
                bookTitleOld = em.merge(bookTitleOld);
            }
            if (bookTitleNew != null && !bookTitleNew.equals(bookTitleOld)) {
                bookTitleNew.getUsersCollection().add(users);
                bookTitleNew = em.merge(bookTitleNew);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = users.getEmail();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(String id) throws NonexistentEntityException, RollbackFailureException, Exception {
        
        try {
            utx.begin();
            
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getEmail();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            Books bookTitle = users.getBookTitle();
            if (bookTitle != null) {
                bookTitle.getUsersCollection().remove(users);
                bookTitle = em.merge(bookTitle);
            }
            em.remove(users);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        
       
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
      
    }

    public Users findUsers(String id) {
        
       
            return em.find(Users.class, id);
        
    }

    public int getUsersCount() {
        
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
       
    }
    
}
